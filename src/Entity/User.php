<?php
// src/App/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    protected $name;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $twitch_id;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $twitch_access_token;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwitchId()
    {
        return $this->twitch_id;
    }

    /**
     * @param mixed $twitch_id
     * @return User
     */
    public function setTwitchId($twitch_id)
    {
        $this->twitch_id = $twitch_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwitchAccessToken()
    {
        return $this->twitch_access_token;
    }

    /**
     * @param mixed $twitch_access_token
     * @return User
     */
    public function setTwitchAccessToken($twitch_access_token)
    {
        $this->twitch_access_token = $twitch_access_token;
        return $this;
    }




}