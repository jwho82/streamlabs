<?php
namespace App\Security;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;

class FOSUBUserProvider extends BaseClass
{

    private $container;
    private $logger;

    public function __construct(UserManagerInterface $userManager, ContainerInterface $container, array $properties)
    {


        $this->userManager = $userManager;
        $this->properties = $properties;
        $this->container = $container; // Gives us access to logged in user, and session flashbag

        $this->properties = [
            'twitch' => 'twitch_id',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        // on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

        // we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        // we connect current user
//        $user->$setter_id($username);
//        $user->$setter_token($response->getAccessToken());
//
//        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {

        $service = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';


        $token = $this->container->get('security.token_storage')->getToken();
        $socialmedia = $response->getUsername();


        // Check to see if user is logged in
        if (!is_null($token)) {
            $user = $token->getUser();

            // Checking to see if social account has already been linked to another account
//            if ($duplicate = $this->userManager->findUserBy(array($service . '_id' => $socialmedia))) {
//
//                // if So, don't let them
//                if ($service == 'facebook' && $duplicate->getFacebookId() != $user->getFacebookId()) {
//                    $this->container->get('session')->getFlashBag()->add(
//                        'error',
//                        'This ' . ucfirst($service) . ' account is already linked to another account!'
//                    );
//
//                    throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
//                }
//
//                if ($service == 'twitter' && $duplicate->getTwitterId() != $user->getTwitterId()) {
//                    $this->container->get('session')->getFlashBag()->add(
//                        'error',
//                        'This ' . ucfirst($service) . ' account is already linked to another account!'
//                    );
//
//                    throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
//                }
//
////                if ($service == 'slack' && $duplicate->getSlackId() != $user->getSlackId()) {
////                    $this->container->get('session')->getFlashBag()->add(
////                        'error',
////                        'This ' . ucfirst($service) . ' account is already linked to another account!'
////                    );
////
////                    throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
////                }
//
//                if ($service == 'google' && $duplicate->getGoogleId() != $user->getGoogleId()) {
//                    $this->container->get('session')->getFlashBag()->add(
//                        'error',
//                        'This ' . ucfirst($service) . ' account is already linked to another account!'
//                    );
//
//                    throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
//                }
//
//                if ($service == 'linkedin' && $duplicate->getLinkedinId() != $user->getLinkedinId()) {
//                    $this->container->get('session')->getFlashBag()->add(
//                        'error',
//                        'This ' . ucfirst($service) . ' account is already linked to another account!'
//                    );
//
//                    throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
//                }
//            }

            // Linking account to existing user
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $request->cookies->set('connect', new Cookie('connect'));

            // Clearing flash. Multiple messages if user enters existing email address.
            $this->container->get('session')->getFlashBag()->clear();

            $this->container->get('session')->getFlashBag()->add(
                'success',
                'We have now linked your account with your  ' . ucfirst($service) . ' account!'
            );

        } else {
            $user = $this->userManager->findUserBy(array($this->getProperty($response) => $socialmedia));
        }
        if (null === $user) {

            // when the user is Registering

            if ($temp = $this->userManager->findUserByEmail($response->getEmail())) {
                $message = 'There is already an account with this email address. Please login with the account first and then connect it';

//                $this->logger->warning('Social Media email already exists '.$response->getEmail());
                $this->container->get('session')->getFlashBag()->add(
                    'error',
                    $message
                );
                throw new \Symfony\Component\Security\Core\Exception\AuthenticationException($message);

            }


            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($socialmedia);
            $user->$setter_token($response->getAccessToken());


            // Splitting up realname
            $name = $response->getNickname();
            $username = $email = $response->getEmail();

            $user->setUsername($username);

            // Twitter does not provide a email
            if ($email == '') {
                // $user->setUsernameCanonical($token);
                $user->setEmail($socialmedia);
                $user->setReset(true);

            } else {
                $user->setEmail($email);
            }

            $user->setPlainPassword($socialmedia);
            $user->setEnabled(true);
            $user->setName($name);
//            $user->setUsername($username);

            $this->userManager->updateUser($user);

            $response = new Response();
            $response->headers->setCookie(new Cookie("new", true));
            $response->headers->clearCookie('ref');
            $response->send();


            return $user;
        }

        $user->$setter_id($socialmedia);
        $user->$setter_token($response->getAccessToken());


        return $user;
    }

}