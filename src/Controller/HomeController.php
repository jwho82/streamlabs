<?php

namespace App\Controller;

use NewTwitchApi\HelixGuzzleClient;
use NewTwitchApi\NewTwitchApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        if ($this->getUser())
            return $this->redirectToRoute('view');

        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/view", name="view")
     */
    public function view(Request $request)
    {
        $channel = $request->query->get('channel');

        if (!$channel)
            return $this->render('home/channel.html.twig');


        $clientId = getenv('TWITCH_ID');
        $options = [
            'client_id' => $clientId,
            'timeout' => 10,
        ];

        $accessToken = $this->getUser()->getTwitchAccessToken();
        $twitchApi = new \TwitchApi\TwitchApi($options);
        $twitchApi->setTimeout(10);

        $user = $twitchApi->getUserByUsername($channel);

        if (!isset($user['users'][0]))
            return $this->render('home/channel.html.twig', ['error' => 'Cannot find a channel for '.$channel]);

//        $id = $user['users'][0]['_id'];
//        $broadcast = $twitchApi->getLiveStreams($id);
//        $topics = [
//            "channel-bits-events-v1.$id",
////            "channel-bits-badge-unlocks.$id",
////            "channel-subscribe-events-v1.$id",
//        ];

        return $this->render('home/app.html.twig', [
            'client_id' => $clientId,
            'channel' => $channel,
            'access_token' => $accessToken,
//            'topics' => implode(',',$topics),
        ]);

    }

    /**
     * Testing Webhooks
     */
    public function view2() {
        $clientId = getenv('TWITCH_ID');
        $clientSecret = getenv('TWITCH_SECRET');
        $accessToken = $this->getUser()->getTwitchAccessToken();

        $helixGuzzleClient = new HelixGuzzleClient($clientId);
        $newTwitchApi = new NewTwitchApi($helixGuzzleClient, $clientId, $clientSecret);

        $channelId = "37402112";

        $newTwitchApi->getWebhooksSubscriptionApi()->subscribeToStream(
            $channelId,
            'bearer-token',
            'http://requestbin.fullcontact.com/12s15qk1',
            1000
        );

        exit;

        try {
            // Make the API call. A ResponseInterface object is returned.
            $response = $newTwitchApi->getUsersApi()->getUserByAccessToken($accessToken);
        } catch (GuzzleException $e) {
            // Handle error appropriately for your application
        }

    }



}